class_name SummonCircle
extends Node3D

@export var pull_force = 1.0
var demon_prefab: PackedScene
var dead_bodies: Array[RigidBody3D]

func _physics_process(_delta):
	for deadBody in dead_bodies:
		deadBody.apply_central_force((global_position - deadBody.global_position) * pull_force)

func _on_animated_sprite_3d_animation_finished():
	
	# Delete bodies
	for deadBody in dead_bodies:
		deadBody.queue_free()
	
	# Summon demon
	var demon = demon_prefab.instantiate() as Node3D
	add_sibling(demon)
	demon.global_position = global_position
	
	# Add screen shake
	var screen_shake = get_tree().get_first_node_in_group("screen_shake") as ScreenShake
	screen_shake.add_shake(1)
	
	queue_free()
