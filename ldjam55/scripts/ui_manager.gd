extends Control

func set_start_screen():
	hide_all()
	%StartScreen.show()

func set_credits():
	hide_all()
	%Credits.show()

func set_fail_screen():
	hide_all()
	%FailScreen.show()

func set_success_screen():
	hide_all()
	%SuccessScreen.show()

func set_hud():
	hide_all()
	%Hud.show()

func hide_all():
	%StartScreen.hide()
	%Credits.hide()
	%FailScreen.hide()
	%SuccessScreen.hide()
	%Hud.hide()
