class_name Citizen
extends CharacterBody3D

var arrayEnemiesInRange = []
const POW_THREAT=10

@export var _bDebug : bool = false
@export var dead_prefab: PackedScene
@export var SPEED_WALK= 5
@export var SPEED_RUN= SPEED_WALK * 4
const GRAVITY : Vector3 = Vector3(0,9.84,0)
const COLOR_ESCAPE : Color = Color(0,1,0,1)
const TIMER_KEEP_ESCAPING = 3

enum STATE_CITIZEN {IDLE, ESCAPING}
var State = STATE_CITIZEN.IDLE
var _vLastDirection : Vector3 = Vector3.RIGHT
var MaterialEscape
#ESCAPE
var _fRemainingTimeEscape : float = 0
#IDLE
@export var fTimerMinBeforeChangeDirection : float = 2
@export var fTimerMaxBeforeChangeDirection : float = 5
@export var fMinSpeedWalk : float = 2
@export var fMaxSpeedWalk : float = 5
@onready var _fSpeed = random.randf_range(fMinSpeedWalk, fMaxSpeedWalk)
@onready var _fDotAuthorizedAvoidWall : float = cos(-3 * PI/4)

@export var animation_player: AnimationPlayer
@export var flip_node: Node3D

var _fRemainingTimeBeforeChangeDirection : float = 0

@export var Link_Human : Link_HumanDie

var random = RandomNumberGenerator.new()

func _ready():
	# MaterialEscape = (%Mesh as MeshInstance3D).get_active_material(0).duplicate()
	# MaterialEscape.albedo_color = COLOR_ESCAPE
	pass

func _on_area_3d_body_entered(body: PhysicsBody3D):
	arrayEnemiesInRange.append(body)


func _on_area_3d_body_exited(body: PhysicsBody3D):
	arrayEnemiesInRange.erase(body)

func _physics_process(delta):
	var vDirectionEscape = Vector3()

	for i in arrayEnemiesInRange.size():
		var enemy : PhysicsBody3D = arrayEnemiesInRange[i]
		var vEnemyRelativePosition : Vector3 = enemy.global_position - global_position
		var fRadiusDetection = %DetectionArea3D.get_node("CollisionShape3D").shape.radius
		var fDistanceEnemy = vEnemyRelativePosition.length()
		var fComputeThreat = clamp(1 - fDistanceEnemy / fRadiusDetection, 0, 1)
		var fThreatWeight = pow(fComputeThreat, POW_THREAT)
		vDirectionEscape -= vEnemyRelativePosition.normalized() * fThreatWeight
		vDirectionEscape.y = 0
	
	var vDirectionEscapeNormalized = vDirectionEscape.normalized()


	if vDirectionEscapeNormalized.length() > 0:
		if State != STATE_CITIZEN.ESCAPING :
			# (%Mesh as MeshInstance3D).set_surface_override_material(0, MaterialEscape) 
			_fSpeed = SPEED_RUN
			State = STATE_CITIZEN.ESCAPING	
			
		_vLastDirection = vDirectionEscapeNormalized
		_fRemainingTimeEscape = TIMER_KEEP_ESCAPING		
	
	else:
		match State :
			STATE_CITIZEN.ESCAPING:
				if _fRemainingTimeEscape >= 0:
					_fSpeed = SPEED_RUN
					_fRemainingTimeEscape -= delta
				
				else :
					_fSpeed = random.randf_range(fMinSpeedWalk, fMaxSpeedWalk)
					State = STATE_CITIZEN.IDLE
					# (%Mesh as MeshInstance3D).set_surface_override_material(0, null)
					
			STATE_CITIZEN.IDLE:
				_fRemainingTimeBeforeChangeDirection -= delta
				if(_fRemainingTimeBeforeChangeDirection <= 0):
					_add_new_timer()
					_vLastDirection = Vector3((random.randf()-0.5)*2, 0, (random.randf()-0.5)*2)
					_vLastDirection = _vLastDirection.normalized()

	var vAvoidWall = %avoidance_wall.GetComputeAvoidanceWall(global_position, %TransformRotation.global_basis.get_rotation_quaternion()
	 ,velocity.length(), _bDebug)

	
	if(_bDebug):
		Draw.Draw_Line_3D(global_position, global_position + vAvoidWall * 2, Color.BLUE)
			
	if vAvoidWall != Vector3.ZERO && (vDirectionEscapeNormalized.length() == 0 || vAvoidWall.dot(_vLastDirection) > _fDotAuthorizedAvoidWall):
		_vLastDirection = vAvoidWall	
		_add_new_timer()	

	velocity = _vLastDirection * _fSpeed
	%TransformRotation.look_at(global_position + velocity.normalized()*10)

	if animation_player != null:
		if State == STATE_CITIZEN.IDLE:
			animation_player.play("Walking")
			animation_player.speed_scale = 1
		else:
			animation_player.play("Walking")
			animation_player.speed_scale = 3

		if flip_node != null:
			if velocity.z > 0:
				flip_node.rotation_degrees.y = 0
			if velocity.z < 0:
				flip_node.rotation_degrees.y = 180
	
	velocity -= GRAVITY

	move_and_slide()

func _on_health_health_depleted():
	if dead_prefab != null:
		var dead_body = dead_prefab.instantiate() as Node3D
		add_sibling(dead_body)
		dead_body.position = position
		dead_body.rotation = rotation
		Link_Human.OnDead(dead_body)
	queue_free()

func _add_new_timer():
	_fRemainingTimeBeforeChangeDirection = random.randf_range(fTimerMinBeforeChangeDirection, fTimerMaxBeforeChangeDirection)				
