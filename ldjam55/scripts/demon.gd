extends CharacterBody3D

@export var wander_range = 5.0
@export var follow_speed = 1.0
@export var wander_speed = 0.5
@export var reach_distance = 0.5
@export var animation_player: AnimationPlayer
@export var flip_node: Node3D
@export var life_time = 10.0

@onready var navigation_agent: NavigationAgent3D = $NavigationAgent3D

enum State { WAIT, WANDER, FOLLOW }

var state = State.WAIT

var wait_time = 0.0;
var target_postion = Vector3.ZERO
var follow_obj: Node3D

func _physics_process(delta):
	
	life_time -= delta
	
	if life_time <= 0:
		queue_free()
	
	if state != State.FOLLOW:
		try_find_new_target()
	
	match state:
		State.WAIT:
			velocity = Vector3.ZERO
			wait_time -= delta
			if wait_time <= 0.0:
				state = State.WANDER
				target_postion = global_position + Vector3(randf_range(-wander_range, wander_range), 0.0, randf_range(-wander_range, wander_range))
		State.WANDER:
			if global_position.distance_squared_to(target_postion) < reach_distance * reach_distance:
				velocity = Vector3.ZERO
				state = State.WAIT
				wait_time = randf_range(1.0, 4.0)
			else:
				velocity = (target_postion - global_position).normalized() * wander_speed
		State.FOLLOW:
			if follow_obj == null:
				velocity = Vector3.ZERO
				state = State.WAIT
				wait_time = randf_range(0.5, 1.0)
			else:
				follow_target()
	
	if state == State.WAIT:
		if animation_player != null:
			animation_player.play("RESET")
	else:
		if animation_player != null:
			animation_player.play("Walking")
	
	if flip_node != null:
		if velocity.z > 0:
			flip_node.rotation_degrees.y = 0
		if velocity.z < 0:
			flip_node.rotation_degrees.y = 180

	move_and_slide()
	
func follow_target():
	var target_dist = global_position.distance_squared_to(follow_obj.global_position)
	if target_dist < reach_distance * reach_distance:
		velocity = Vector3.ZERO
		return
	
	var space_state = get_world_3d().direct_space_state
	var ray_query = PhysicsRayQueryParameters3D.create(global_position + Vector3.UP * 0.8, follow_obj.global_position + Vector3.UP * 0.8, 1)
	
	var result_result = space_state.intersect_ray(ray_query)
	
	var next_pos
	if result_result:
		if navigation_agent.is_navigation_finished():
			navigation_agent.target_position = follow_obj.global_position
		next_pos = navigation_agent.get_next_path_position()
	else:
		next_pos = follow_obj.global_position
	velocity = global_position.direction_to(next_pos) * follow_speed

func try_find_new_target():
	var nearest_target = null
	var min_dist = 1000000000000.0
	for human in get_tree().get_nodes_in_group("human"):
		var dist = global_position.distance_squared_to(human.global_position)
		if dist < min_dist:
			nearest_target = human
			min_dist = dist
	
	if nearest_target != null:
		follow_obj = nearest_target
		navigation_agent.target_position = nearest_target.global_position
		state = State.FOLLOW
