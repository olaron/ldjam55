class_name GameStateMgr
extends Node

@export var levels: Array[PackedScene]
@export var current_level = -1
var current_loaded_level: Node

var tool_bar: ToolBar

signal reset
signal start
signal victory
signal defeat

enum State { NOT_STARTED, RUNNING, VICTORY, DEFEAT }
var state = State.NOT_STARTED

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if current_level == -1:
		return
	
	if state == State.RUNNING:
		if get_tree().get_first_node_in_group("human") == null:
			victory.emit()
			state = State.VICTORY
			current_level += 1
		elif get_tree().get_first_node_in_group("demon") == null and tool_bar != null and tool_bar.is_empty():
			defeat.emit()
			state = State.DEFEAT

func start_gameplay():
	if state == State.NOT_STARTED:
		state = State.RUNNING
		start.emit()

func start_game():
	current_level = 0
	load_level()

func load_level():
	if current_loaded_level != null:
		current_loaded_level.queue_free()
		current_loaded_level = null
	
	if current_level < levels.size():
		current_loaded_level = levels[current_level].instantiate()
		add_sibling(current_loaded_level)
	my_reset()
	reset.emit()

func my_reset():
	state = State.NOT_STARTED
	tool_bar = get_tree().get_first_node_in_group("tool_bar")


func quit_game():
	get_tree().quit()
