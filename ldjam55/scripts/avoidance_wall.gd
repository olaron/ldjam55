class_name avoidance_wall
extends Node3D


@export var fRadiusStart : float = 1
@export var fRadiusEnd : float = 2
@export var fAngleRay : float = PI/2
@export var fMinRayCast : float = 0.1

@onready var _QuaternionRotate : Quaternion = Quaternion(Vector3.UP, fAngleRay)


func GetComputeAvoidanceWall(positionToTest : Vector3, rotationToTest : Quaternion, velocityToTest : float, bDebug : bool = false) -> Vector3:
	assert( fRadiusEnd > fRadiusStart, "ERROR: You must give an end bigger than start");
	assert( fMinRayCast < fRadiusEnd - fRadiusStart, "ERROR: You must give a min length smaller than radius");
	
	var space_state = get_world_3d().direct_space_state
	var vFront : Vector3 = rotationToTest * Vector3.FORWARD
	vFront = vFront.normalized()
	var vpositionToTest = positionToTest + Vector3.UP * 0.5
	
	var ray = vFront
	var fAngle = 0
	var bIsBlocked = false
	var vOut = Vector3.ZERO
	while fAngle < 2*PI:
		ray = _QuaternionRotate * ray
		ray = ray.normalized()
		var dot = (ray.dot(vFront) + 1)/2
		var length = max(dot, fMinRayCast)
		var vStart : Vector3 = vpositionToTest + ray * fRadiusStart
		var vEnd : Vector3 = vpositionToTest + ray * clamp(fRadiusStart + (fRadiusEnd-fRadiusStart) * length * max(velocityToTest, 1), 0, fRadiusEnd)
		var query = PhysicsRayQueryParameters3D.create(vStart, vEnd, 1, [self])
		var result :  = space_state.intersect_ray(query)
		
		if(!result):
			if bDebug:
				Draw.Draw_Line_3D(vStart, vEnd, Color.GREEN)
			fAngle+= fAngleRay
			continue
		
		var directionHit = result.position - vpositionToTest
		if bDebug:
			Draw.Draw_Line_3D(vStart, result.position, Color.RED)
		
		var wantedDir = Vector3.ZERO;
		if(dot >= 0.99):
			bIsBlocked = true
		elif(dot > 0.707):
			var vNormal = result.normal
			var crossProduct = vNormal.cross(Vector3.UP)
			wantedDir = crossProduct.normalized() * length
		elif(dot > 0):
			var quaternionRotateSymetry : Quaternion = Quaternion(vFront, PI)
			wantedDir =  (quaternionRotateSymetry * directionHit).normalized() * length
		fAngle+= fAngleRay
		
		vOut += wantedDir
		
	if(bIsBlocked):
		if(vOut == Vector3.ZERO):
			vOut = -vFront
		else:
			vOut -= vFront
			
	vOut.normalized()
	return vOut
