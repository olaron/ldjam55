extends Node

@export
var sun_light: Node

func dark():
	sun_light.hide()

func light():
	sun_light.show()
