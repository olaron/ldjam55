extends Node

@export var enable_prost_processing = true

func setup():
	if enable_prost_processing:
		var camera = get_tree().get_first_node_in_group("camera")
		if camera:
			camera.get_node("PostProcess").show()
