class_name ManagerSpawnDemon
extends Node

@export var m_fRadiusBodies : float = 10
@export var summoning_prefab: PackedScene
@export var proto : Array[DemonPropertySpawn]


var m_daDeadHuman = {}

@export var COOLDOWN_SPAWN_DEMON : float = 5

@export var m_Link : Link_HumanDie

func _ready():
	m_Link.m_ManagerSpawnDemon = self

func OnDeadHuman(body):
	m_daDeadHuman[body] = Time.get_ticks_msec() + COOLDOWN_SPAWN_DEMON * 1000
	
#func _process(delta):
	#var daTimeout : Array[Node]
	#
	#for node in m_daDeadHuman:
		#if m_daDeadHuman[node] <= Time.get_ticks_msec():
			#daTimeout.append(node)
	#
	#for node in daTimeout:
		#m_daDeadHuman.erase(node)
		#
			
