class_name ToolBar
extends Node

@export var camera: Camera3D
@export var demon_parent: Node
@export var demon_prefab: PackedScene
@export var demon_count = 4

func _input(event):
	if !(event is InputEventMouseButton and event.pressed and event.button_index == 1):
		return
		
	if demon_count <= 0:
		return
	
	demon_count -= 1
	
	var origin = camera.project_ray_origin(event.position)
	var normal = camera.project_ray_normal(event.position)
	var projection = origin - normal * (origin.y / normal.y)
	projection.y = 0.0
	
	var new_demon = demon_prefab.instantiate() as Node3D
	demon_parent.add_child(new_demon)
	new_demon.global_position = projection
	
	var game = get_tree().get_first_node_in_group("game_state_mgr") as GameStateMgr
	if game:
		game.start_gameplay()

func is_empty():
	return demon_count == 0
