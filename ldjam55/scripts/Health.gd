class_name Health
extends Node

@export var m_fHealth = 100


signal health_depleted

func OnAttacked(_fDamage):
	m_fHealth -= _fDamage
	
	if m_fHealth <= 0:
		health_depleted.emit()
	

