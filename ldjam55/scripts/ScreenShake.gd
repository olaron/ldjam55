class_name ScreenShake
extends Node

@export var intensity : float = 0.2;
@export var decay : float = 8.0;

@onready var camera : Camera3D = %Camera3D
var shake : float = 0;

func add_shake(amount : float):
	shake += amount
	
func _process(delta : float):
	shake = shake * exp(-decay * delta)
	var direction = Vector2.from_angle(randf()  * TAU)
	camera.position.x = direction.x * shake * intensity
	camera.position.y = direction.y * shake * intensity
