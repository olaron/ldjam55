extends RigidBody3D

signal destroy_object
@export var centerMass : Vector3
var destroyed = false

func OnDestroy(body : Node3D):
	if destroyed:
		return
	
	axis_lock_angular_x = false
	axis_lock_angular_y = false
	axis_lock_angular_z = false
	axis_lock_linear_x = false
	axis_lock_linear_y = false
	axis_lock_linear_z = false
	center_of_mass = centerMass
	collision_layer = collision_layer & ~1
	
	var direction = global_position - body.global_position
	Draw.Draw_Line_3D(global_position + Vector3.UP, body.global_position + Vector3.UP, Color.RED)
	
	apply_central_impulse(direction.normalized() * 80)
	destroyed = true
	
	destroy_object.emit()
