class_name Attack
extends Node

@export var m_fDamage = 50
@export var m_CoolDownHit = 1
@export var m_iMaxTarget = -1

@export_flags_3d_physics var layers_3d_physics

const _Health = preload("res://scripts/Health.gd")

var _daTargetAvailableNotAttacked : Array[_Health]
var _timer : Timer = null
var _daCurrentTarget : Array[_Health]

func _attack():
	
	
	for n in  range(max(_daCurrentTarget.size()-1, 0), 0, -1):
		if(_daCurrentTarget[n] == null || !is_instance_valid(_daTargetAvailableNotAttacked[n])):
				_daCurrentTarget.remove_at(n)

	if m_iMaxTarget < 0:
		for n in  range(max(_daTargetAvailableNotAttacked.size()-1, 0), -1, -1):
			if(_daTargetAvailableNotAttacked[n] == null || !is_instance_valid(_daTargetAvailableNotAttacked[n])):
					_daTargetAvailableNotAttacked.remove_at(n)
					continue
			_daTargetAvailableNotAttacked[n].OnAttacked(m_fDamage)
	
	else:
		if _daCurrentTarget.size() < m_iMaxTarget:
			for n in  range(_daCurrentTarget.size(), min(_daTargetAvailableNotAttacked.size(), m_iMaxTarget)):				
				_daCurrentTarget.append(_daTargetAvailableNotAttacked[0])
				_daTargetAvailableNotAttacked.remove_at(0)
		
		for n in  range(max(_daCurrentTarget.size()-1, -1), -1, -1):
			if(_daCurrentTarget[n] == null || !is_instance_valid(_daCurrentTarget[n])):
					_daCurrentTarget.remove_at(n)
					continue
					
			_daCurrentTarget[n].OnAttacked(m_fDamage)
	
	if _daTargetAvailableNotAttacked.size() == 0 && _daCurrentTarget.size() == 0 && _timer != null:
		_timer.stop()
		
func _on_timeout():
	_attack()

func _on_attack_area_body_entered(body: Node3D):
	var collider:CollisionObject3D = body as CollisionObject3D
	if not is_instance_valid(collider):
		return

	if layers_3d_physics & collider.collision_layer == 0:
		return
		
	var healthTarget = body.get_node("Health") as _Health	
	if healthTarget != null:
		_daTargetAvailableNotAttacked.append(healthTarget)
		if(_timer == null || _timer.is_stopped()):
			_attack()
			
		if(_timer == null):
			_timer = Timer.new()
			add_child(_timer)
			_timer.wait_time = m_CoolDownHit
			_timer.one_shot = false
			_timer.connect("timeout", self._on_timeout, 0)
			
		if(_timer.is_stopped()):
			_timer.start()
			
			

			

func _on_attack_area_body_exited(body: Node3D):
	var collider:CollisionObject3D = body as CollisionObject3D
	if not is_instance_valid(collider):
		return

	if layers_3d_physics & collider.collision_layer == 0:
		return
		
	var healthTarget = body.get_node("Health") as _Health	
	if healthTarget != null:
		_daTargetAvailableNotAttacked.erase(healthTarget)
		_daCurrentTarget.erase(healthTarget)
		
		if _daTargetAvailableNotAttacked.size() == 0 && _daCurrentTarget.size() == 0 && _timer != null:
			_timer.stop()
