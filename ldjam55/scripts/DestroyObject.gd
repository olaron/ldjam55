class_name DestroyObject
extends Node3D

@export_flags_3d_physics var layers_3d_physics

func OnCollideDestroyableObject(body : Node3D):
	var collider:CollisionObject3D = body as CollisionObject3D
	if not is_instance_valid(collider):
		return

	if layers_3d_physics & collider.collision_layer > 0:
		var target = collider.find_children("*", "DestroyInteraction")
		if(target.size() > 0):
			var destroyInteractionTarget : DestroyInteraction = target[0] as DestroyInteraction
			
			if  destroyInteractionTarget:
				destroyInteractionTarget.DestroyInteraction(self)
