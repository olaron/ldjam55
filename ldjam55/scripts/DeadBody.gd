class_name DeadBody
extends Node3D

@export var TIMER : float
@export var link_humanDie : Link_HumanDie

var _unspawn = false
var _m_timer : Timer = null

func WillUnSpawn() -> bool :
	return _unspawn

func Unspawn() -> void :
	_unspawn = true
	remove_child(_m_timer)
	_m_timer = null

func _ready():
	_m_timer = Timer.new()
	add_child(_m_timer)
	_m_timer.wait_time = TIMER
	_m_timer.one_shot = true
	_m_timer.connect("timeout", self._timeout, 0)
	_m_timer.start()

func _timeout():
	if _unspawn:
		pass
			
	var daBodies = (%Area3D as Area3D).get_overlapping_bodies()
	
	var daValidBodies: Array[RigidBody3D] = []
	for body in daBodies:
		var deadBody = body as DeadBody
		
		if deadBody && !deadBody.WillUnSpawn():
			daValidBodies.append(deadBody)
	
	var LastValidPrefab : DemonPropertySpawn = null
	for item in link_humanDie.m_ManagerSpawnDemon.proto:
		if item.nbDeadBodies > daValidBodies.size():
			break
		else:
			LastValidPrefab = item
	
	if LastValidPrefab:
		# Sort by distance
		daValidBodies.sort_custom(func(a, b): return a.global_position.distance_squared_to(global_position) < b.global_position.distance_squared_to(global_position))
		# Only take the desired amount of bodies
		daValidBodies.resize(LastValidPrefab.nbDeadBodies)
		
		var pos = Vector3.ZERO
		
		for deadBody in daValidBodies:
			deadBody.Unspawn()
			pos += deadBody.global_position
		
		var summon_circle = link_humanDie.m_ManagerSpawnDemon.summoning_prefab.instantiate() as SummonCircle
		summon_circle.demon_prefab = LastValidPrefab.prefab
		summon_circle.dead_bodies = daValidBodies
		add_sibling(summon_circle)
		summon_circle.global_position = pos / daValidBodies.size() # Center of the bodies
	else:
		queue_free()
	
