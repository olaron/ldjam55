class_name DestroyInteraction
extends Node

signal destroy_interaction

func DestroyInteraction(body : Node3D):
	destroy_interaction.emit(body)
