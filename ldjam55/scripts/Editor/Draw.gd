extends Node

var _daMeshes : Array[MeshInstance3D]
var _daNextMeshes : Array[MeshInstance3D]
var _daNextNextMeshes : Array[MeshInstance3D]

func _ready():
	process_priority = 1

func Draw_Line_3D(start : Vector3, end : Vector3, color : Color) :
	var mesh : ImmediateMesh = ImmediateMesh.new()
	mesh.surface_begin(Mesh.PRIMITIVE_LINES)
	mesh.surface_set_color(color)
	mesh.surface_add_vertex(start)
	mesh.surface_set_color(color)
	mesh.surface_set_color(color)
	mesh.surface_add_vertex(end)
	mesh.surface_set_color(color)
	mesh.surface_end()
	var material : StandardMaterial3D = StandardMaterial3D.new()
	material.albedo_color = color
	var  meshinstance : MeshInstance3D= MeshInstance3D.new();	
	meshinstance.mesh = mesh	
	meshinstance.set_surface_override_material(0, material)
	get_tree().root.add_child(meshinstance)
	_daNextNextMeshes.append(meshinstance)
	
func _process(delta):
	for mesh in _daMeshes:
		mesh.queue_free()
		
	_daMeshes = _daNextMeshes
	_daNextMeshes = _daNextNextMeshes
	_daNextNextMeshes = []
	

