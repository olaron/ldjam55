class_name Music
extends AudioStreamPlayer

@export
var base_stream: AudioStream

@export
var panic_stream: AudioStream

@export
var end_panic_stream: AudioStream

func start_panic():
	stream = panic_stream
	play()

func end_panic():
	stream = end_panic_stream
	play()

func reset():
	stream = base_stream
	play()
